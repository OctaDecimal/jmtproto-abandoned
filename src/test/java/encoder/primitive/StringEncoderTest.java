package encoder.primitive;

import decoder.primitive.StringDecoder;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;

/**
 * If you see it, than I've forgotten javadoc
 *
 * @author Denis Golovachev
 * @author $Author$ (current maintainer)
 * @since 1.0
 */
public class StringEncoderTest {
    @Test
    public void testEncode() throws Exception {
        String[] testStr = {"abc", "tdd", "agile", "java"};
        for (String str: testStr) {
            byte[] encoded = new StringEncoder().encode(str);
            String decoded = new StringDecoder().decode(new ByteArrayInputStream(encoded));
            Assert.assertEquals(str, decoded);
        }
    }


    @Test
    public void testLongStringEncode() throws Exception {
        String testStr =
                "abcaasadasdasdasdaahsdlffsdafdsf32342nadsjasfasdfasdfasdjh2342323234adasdfdsacvafdfphjjhjladf" +
                        "sdafasdfsdfasfasdfasdfasdfasfsadfsdfasfwqerqwrqwerwrqrqwrewqwerwqrwqerwqrwrqwrweqrwerqwr" +
                        "qwerwqerwqrwerwqerewqrqwerqwerqwrwqrweqwrwqrwqerqwrwqrqwerqwr211222ssssssssssssssssssss";
        byte[] encoded = new StringEncoder().encode(testStr);
        String decoded = new StringDecoder().decode(new ByteArrayInputStream(encoded));
        Assert.assertEquals(testStr, decoded);
    }

}
