package encoder.primitive;

import decoder.primitive.IntDecoder;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;

/**
 * If you see it, than I've forgotten javadoc
 *
 * @author Denis Golovachev
 * @author $Author$ (current maintainer)
 * @since 1.0
 */
public class IntEncoderTest {
    @Test
    public void testEncode() throws Exception {
        Integer num = 54;
        byte[] encoded = new IntEncoder().encode(num);
        Assert.assertEquals(num, new IntDecoder().decode(new ByteArrayInputStream(encoded)));
    }

    @Test
    public void testEncode2() throws Exception {
        Integer num = 123455634;
        byte[] encoded = new IntEncoder().encode(num);
        Assert.assertEquals(num, new IntDecoder().decode(new ByteArrayInputStream(encoded)));
    }
}
