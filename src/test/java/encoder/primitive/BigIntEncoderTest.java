package encoder.primitive;

import decoder.primitive.NumberDecoder;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.math.BigInteger;

/**
 * If you see it, than I've forgotten javadoc
 *
 * @author Denis Golovachev
 * @author $Author$ (current maintainer)
 * @since 1.0
 */
public class BigIntEncoderTest {
    @Test
    public void testEncode() throws Exception {
        BigInteger num = new BigInteger
                ("34833033790981183642218815852910710788937631063684158965387798388844821552177");
        byte[] encoded = new BigIntEncoder().encode(num);
        Assert.assertEquals(num.toString(), new NumberDecoder(256).decode(new ByteArrayInputStream(encoded)).toString());
    }
}
