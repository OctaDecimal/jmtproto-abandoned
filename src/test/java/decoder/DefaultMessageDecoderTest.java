package decoder;

import decoder.commands.PqAuthReq;
import decoder.commands.PqAuthResp;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang.ArrayUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.math.BigInteger;

/**
 * If you see it, than I've forgotten javadoc
 *
 * @author Denis Golovachev
 * @author $Author$ (current maintainer)
 * @since 1.0
 */
public class DefaultMessageDecoderTest {

    String nonce = "3e0549828cca27e966b301a48fece2fc";
    String pqAuth = "000000000000000000000000C47AE551" +
                    "1400000078974660" + nonce;

    @Test
    public void testDecode() throws Exception {
        HeaderMessageDecoder messageDecoder = new HeaderMessageDecoder();
        messageDecoder.setNonEncryptedDecoder(new ContentMessageDecoder());
        PqAuthReq auth = messageDecoder.decode(new ByteArrayInputStream(Hex.decodeHex(pqAuth.toCharArray())),
                PqAuthReq.class);
        byte[] nonceBytes = ((BigInteger) auth.getNonce()).toByteArray();
        ArrayUtils.reverse(nonceBytes); // reversed, coz it's decoded as number
        String nonceInHex = Hex.encodeHexString(nonceBytes);
        Assert.assertNotNull(auth.getNonce());
        Assert.assertEquals(nonce, nonceInHex);
    }

    String pqResp = "000000000000000001C8831EC97AE551"+
                    "40000000632416053E0549828CCA27E9"+
                    "66B301A48FECE2FCA5CF4D33F4A11EA8"+
                    "77BA4AA5739073300817ED48941A08F9"+
                    "810000004EA634C701000000216BE86C"+
                    "022BB4C3";

    @Test
    public void testDecodePQResponse() throws Exception {
        HeaderMessageDecoder messageDecoder = new HeaderMessageDecoder();
        messageDecoder.setNonEncryptedDecoder(new ContentMessageDecoder());
        PqAuthResp auth = messageDecoder.decode(new ByteArrayInputStream(Hex.decodeHex(pqResp.toCharArray())),
                PqAuthResp.class);
        byte[] nonceBytes = ((BigInteger) auth.getNonce()).toByteArray();
        ArrayUtils.reverse(nonceBytes); // reversed, coz it's decoded as number
        String nonceInHex = Hex.encodeHexString(nonceBytes);
        Assert.assertNotNull(auth.getNonce());
        Assert.assertEquals(nonce, nonceInHex); //TODO: This fails because of alignment
    }
}
