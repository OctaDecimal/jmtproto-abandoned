package decoder.primitive;

import org.apache.commons.codec.binary.Hex;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;

/**
 * If you see it, than I've forgotten javadoc
 *
 * @author Denis Golovachev
 * @author $Author$ (current maintainer)
 * @since 1.0
 */
public class IntDecoderTest {

    @Test
    public void decode32BitInteger() throws Exception {
        String string = "0817ED48";
        Integer decoded = new IntDecoder().decode(new ByteArrayInputStream(Hex.decodeHex(string.toCharArray())));
        Assert.assertEquals("1223497480", decoded.toString());
    }
}
