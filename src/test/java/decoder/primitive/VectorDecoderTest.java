package decoder.primitive;

import org.apache.commons.codec.binary.Hex;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.util.Vector;

/**
 * If you see it, than I've forgotten javadoc
 *
 * @author Denis Golovachev
 * @author $Author$ (current maintainer)
 * @since 1.0
 */
public class VectorDecoderTest {
    @Test
    public void testDecode() throws Exception {
        String vector = "4EA634C701000000216BE86C022BB4C3";
        @SuppressWarnings("unchecked")
        Vector<Long> decoded = new VectorDecoder().decode(new ByteArrayInputStream(Hex.decodeHex(vector.toCharArray
                ())
        ));
        Assert.assertEquals(1, decoded.size());
        Assert.assertEquals(new Long(-4344800451088585951l), decoded.get(0));
    }
}
