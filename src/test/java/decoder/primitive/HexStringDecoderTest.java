package decoder.primitive;

import org.apache.commons.codec.binary.Hex;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;

/**
 * If you see it, than I've forgotten javadoc
 *
 * @author Denis Golovachev
 * @author $Author$ (current maintainer)
 * @since 1.0
 */
public class HexStringDecoderTest {

    @Test
    public void decodeStringWithPadding() throws Exception {
        String string = "0817ED48941A08F981000000";
        String decoded = new HexStringDecoder().decode(new ByteArrayInputStream(Hex.decodeHex(string.toCharArray())));
        Assert.assertEquals("17ed48941a08f981", decoded);
    }

    @Test
    public void decodeLongString() throws Exception {
        String string = "FE50020028A92FE2"+
                "0173B347A8BB324B5FAB2667C9A8BBCE"+
                "6468D5B509A4CBDDC186240AC912CF70"+
                "06AF8926DE606A2E74C0493CAA57741E"+
                "6C82451F54D3E068F5CCC49B4444124B"+
                "9666FFB405AAB564A3D01E67F6E91286"+
                "7C8D20D9882707DC330B17B4E0DD57CB"+
                "53BFAAFA9EF5BE76AE6C1B9B6C51E2D6"+
                "502A47C883095C46C81E3BE25F62427B"+
                "585488BB3BF239213BF48EB8FE34C9A0"+
                "26CC8413934043974DB0355663303839"+
                "2CECB51F94824E140B98637730A4BE79"+
                "A8F9DAFA39BAE81E1095849EA4C83467"+
                "C92A3A17D997817C8A7AC61C3FF414DA"+
                "37B7D66E949C0AEC858F048224210FCC"+
                "61F11C3A910B431CCBD104CCCC8DC6D2"+
                "9D4A5D133BE639A4C32BBFF153E63ACA"+
                "3AC52F2E4709B8AE01844B142C1EE89D"+
                "075D64F69A399FEB04E656FE3675A6F8"+
                "F412078F3D0B58DA15311C1A9F8E53B3"+
                "CD6BB5572C294904B726D0BE337E2E21"+
                "977DA26DD6E33270251C2CA29DFCC702"+
                "27F0755F84CFDA9AC4B8DD5F84F1D1EB"+
                "36BA45CDDC70444D8C213E4BD8F63B8A"+
                "B95A2D0B4180DC91283DC063ACFB92D6"+
                "A4E407CDE7C8C69689F77A007441D4A6"+
                "A8384B666502D9B77FC68B5B43CC607E"+
                "60A146223E110FCB43BC3C942EF98193"+
                "0CDC4A1D310C0B64D5E55D308D863251"+
                "AB90502C3E46CC599E886A927CDA963B"+
                "9EB16CE62603B68529EE98F9F5206419"+
                "E03FB458EC4BD9454AA8F6BA777573CC"+
                "54B328895B1DF25EAD9FB4CD5198EE02"+
                "2B2B81F388D281D5E5BC580107CA01A5"+
                "0665C32B552715F335FD76264FAD00DD"+
                "D5AE45B94832AC79CE7C511D194BC42B"+
                "70EFA850BB15C2012C5215CABFE97CE6"+
                "6B8D8734D0EE759A638AF013";

        String decoded = new HexStringDecoder().decode(new ByteArrayInputStream(Hex.decodeHex(string.toCharArray())));

        Assert.assertEquals
                ("28a92fe20173b347a8bb324b5fab2667c9a8bbce6468d5b509a4cbddc186240ac912cf7006af" +
                        "8926de606a2e74c0493caa57741e6c82451f54d3e068f5ccc49b4444124b9666ffb405" +
                        "aab564a3d01e67f6e912867c8d20d9882707dc330b17b4e0dd57cb53bfaafa9ef5be76a" +
                        "e6c1b9b6c51e2d6502a47c883095c46c81e3be25f62427b585488bb3bf239213bf48eb" +
                        "8fe34c9a026cc8413934043974db03556633038392cecb51f94824e140b98637730a4be" +
                        "79a8f9dafa39bae81e1095849ea4c83467c92a3a17d997817c8a7ac61c3ff414da37b7d" +
                        "66e949c0aec858f048224210fcc61f11c3a910b431ccbd104cccc8dc6d29d4a5d133be6" +
                        "39a4c32bbff153e63aca3ac52f2e4709b8ae01844b142c1ee89d075d64f69a399feb04e" +
                        "656fe3675a6f8f412078f3d0b58da15311c1a9f8e53b3cd6bb5572c294904b726d0be337" +
                        "e2e21977da26dd6e33270251c2ca29dfcc70227f0755f84cfda9ac4b8dd5f84f1d1eb36" +
                        "ba45cddc70444d8c213e4bd8f63b8ab95a2d0b4180dc91283dc063acfb92d6a4e407cde7c" +
                        "8c69689f77a007441d4a6a8384b666502d9b77fc68b5b43cc607e60a146223e110fcb43bc3c9" +
                        "42ef981930cdc4a1d310c0b64d5e55d308d863251ab90502c3e46cc599e886a927cda963b9eb16ce" +
                        "62603b68529ee98f9f5206419e03fb458ec4bd9454aa8f6ba777573cc54b328895b1df25ead9fb4cd5198e" +
                        "e022b2b81f388d281d5e5bc580107ca01a50665c32b552715f335fd76264fad00ddd5ae45b94832ac79c" +
                        "e7c511d194bc42b70efa850bb15c2012c5215cabfe97ce66b8d8734d0ee759a638af013",
                        decoded);
    }
}
