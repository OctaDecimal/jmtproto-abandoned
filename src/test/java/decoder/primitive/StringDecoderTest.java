package decoder.primitive;

import api.exceptions.DecodingException;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;

/**
 * If you see it, than I've forgotten javadoc
 *
 * @author Denis Golovachev
 * @author $Author$ (current maintainer)
 * @since 1.0
 */
public class StringDecoderTest {
    @Test
    public void decodeSmallString() throws Exception {
        String string = "04494C553B";
        String decoded = decode(string);
        Assert.assertEquals("ILU;", decoded);
    }

    private String decode(String string) throws DecodingException, DecoderException {
        return new StringDecoder().decode(new ByteArrayInputStream(Hex.decodeHex(string.toCharArray())));
    }

}
