package decoder.primitive;

import org.apache.commons.codec.binary.Hex;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.math.BigInteger;

/**
 * If you see it, than I've forgotten javadoc
 *
 * @author Denis Golovachev
 * @author $Author$ (current maintainer)
 * @since 1.0
 */
public class NumberDecoderTest {
    @Test
    public void testDecode() throws Exception {
        String string = "311C85DB234AA2640AFC4A76A735CF5B1F0FD68BD17FA181E1229AD867CC024D";
        Number number = new NumberDecoder(256).decode(new ByteArrayInputStream(Hex.decodeHex(string.toCharArray()
        )));
        Assert.assertEquals("Should be 256 bit number", 256, ((BigInteger)number).toByteArray().length * 8);
        Assert.assertEquals("34833033790981183642218815852910710788937631063684158965387798388844821552177", number.toString());
    }
}
