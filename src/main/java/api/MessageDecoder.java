package api;

import api.exceptions.DecodingException;

import java.io.InputStream;

/**
 * If you see it, than I've forgotten javadoc
 *
 * @author Denis Golovachev
 * @author $Author$ (current maintainer)
 * @since 1.0
 */
public interface MessageDecoder {

    <T> T decode(InputStream data, Class<T> objClass) throws DecodingException;
}
