package api;

import api.exceptions.EncodingException;

/**
 * If you see it, than I've forgotten javadoc
 *
 * @author Denis Golovachev
 * @author $Author$ (current maintainer)
 * @since 1.0
 */
public interface Encoder<T> {

    byte[] encode(T obj) throws EncodingException;
}
