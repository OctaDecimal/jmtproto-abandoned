package encoder.primitive;

import api.Encoder;
import api.exceptions.EncodingException;
import org.apache.commons.lang.ArrayUtils;

import java.math.BigInteger;

/**
 * If you see it, than I've forgotten javadoc
 *
 * @author Denis Golovachev
 * @author $Author$ (current maintainer)
 * @since 1.0
 */
public class BigIntEncoder implements Encoder<BigInteger> {

    @Override
    public byte[] encode(BigInteger obj) throws EncodingException {
        byte[] objBytes = obj.toByteArray();
        ArrayUtils.reverse(objBytes);
        return objBytes;
    }
}
