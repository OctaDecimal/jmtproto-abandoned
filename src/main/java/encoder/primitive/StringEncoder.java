package encoder.primitive;

import api.Encoder;
import api.exceptions.EncodingException;
import org.apache.commons.lang.ArrayUtils;

/**
 * If you see it, than I've forgotten javadoc
 *
 * @author Denis Golovachev
 * @author $Author$ (current maintainer)
 * @since 1.0
 */
public class StringEncoder implements Encoder<String> {

    private static final byte longStringMarker = (byte)0xFE;

    private static final int longStringBorder = 253;

    @Override
    public byte[] encode(String string) throws EncodingException {
        byte[] byteString = string.getBytes();
        int stringLength = byteString.length;
        byte[] encodedString;
        if(isLongString(byteString)) {
            encodedString = encodeLongString(byteString, stringLength);
        } else {
            encodedString = ArrayUtils.add(byteString, 0, (byte)byteString.length);
        }
        return addPadding(encodedString);
    }

    private byte[] addPadding(byte[] encodedString) {
        int difference = (encodedString.length + 4) % 4;
        int padding = difference > 0 ? 4 - difference: 0; // 32 bit padding
        return ArrayUtils.addAll(encodedString, new byte[padding]);
    }


    private boolean isLongString(byte[] byteString) {
        return byteString.length > longStringBorder;
    }

    private byte[] encodeLongString(byte[] byteString, int stringLength) {
        int thirdSizeByte = stringLength / 65536;
        int secondSizeByte = (stringLength - thirdSizeByte * 65536) / 256;
        int firstSizeByte = ((stringLength - thirdSizeByte * 65536) - secondSizeByte * 256);
        byte[] lengthHeader = {longStringMarker ,(byte)firstSizeByte , (byte)secondSizeByte, (byte)thirdSizeByte};
        return ArrayUtils.addAll(lengthHeader, byteString);
    }
}
