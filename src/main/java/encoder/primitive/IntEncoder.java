package encoder.primitive;

import api.Encoder;
import api.exceptions.EncodingException;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * If you see it, than I've forgotten javadoc
 *
 * @author Denis Golovachev
 * @author $Author$ (current maintainer)
 * @since 1.0
 */
public class IntEncoder implements Encoder<Integer> {


    @Override
    public byte[] encode(Integer obj) throws EncodingException {
        ByteBuffer bb = ByteBuffer.allocate(4);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        bb.putInt(obj);
        return bb.array();
    }
}
