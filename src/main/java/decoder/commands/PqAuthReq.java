package decoder.commands;

import api.annotations.Constructor;
import api.annotations.Size;

/**
 * If you see it, than I've forgotten javadoc
 *
 * @author Denis Golovachev
 * @author $Author$ (current maintainer)
 * @since 1.0
 */
@Constructor(1615239032)
public class PqAuthReq {

    @Size(128)
    Number nonce;

    public Number getNonce() {
        return nonce;
    }

    public void setNonce(Number nonce) {
        this.nonce = nonce;
    }
}
