package decoder.commands;

import api.annotations.Constructor;
import api.annotations.Size;

import java.util.Vector;

/**
 * If you see it, than I've forgotten javadoc
 *
 * @author Denis Golovachev
 * @author $Author$ (current maintainer)
 * @since 1.0
 */
@Constructor(85337187)
public class PqAuthResp {

    @Size(128)
    Number nonce;

    @Size(128)
    Number server_nonce;

    String pq;

    Vector<Long> fingerprints;

    public Vector<Long> getFingerprints() {
        return fingerprints;
    }

    public void setFingerprints(Vector<Long> fingerprints) {
        this.fingerprints = fingerprints;
    }

    public Number getNonce() {
        return nonce;
    }

    public void setNonce(Number nonce) {
        this.nonce = nonce;
    }

    public Number getServer_nonce() {
        return server_nonce;
    }

    public void setServer_nonce(Number server_nonce) {
        this.server_nonce = server_nonce;
    }

    public String getPq() {
        return pq;
    }

    public void setPq(String pq) {
        this.pq = pq;
    }
}
