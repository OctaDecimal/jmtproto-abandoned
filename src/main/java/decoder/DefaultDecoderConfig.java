package decoder;

import api.Decoder;
import decoder.primitive.ChecksumCalculator;
import decoder.primitive.HexStringDecoder;
import decoder.primitive.IntDecoder;
import decoder.primitive.LongDecoder;
import decoder.primitive.VectorDecoder;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

/**
 * If you see it, than I've forgotten javadoc
 *
 * @author Denis Golovachev
 * @author $Author$ (current maintainer)
 * @since 1.0
 */
public class DefaultDecoderConfig {

    public static final Map<Class, Decoder> decodersMap = DefaultDecoderConfig.decodersMap();

    public static Map<Class, Decoder> decodersMap() {
        Map<Class, Decoder> decoderMap = new HashMap<>();
        decoderMap.put(String.class, new HexStringDecoder());
        decoderMap.put(Integer.class, new IntDecoder());
        decoderMap.put(Long.class, new LongDecoder());
        decoderMap.put(Long.class, new LongDecoder());
        decoderMap.put(Vector.class, new VectorDecoder());
        return decoderMap;
    }

    public static Map<Integer, Class> supportedContainers() {
        Map<Integer, Class> constructors = new HashMap<>();
        constructors.put(ChecksumCalculator.calculate("vector # [ string ] = Vector string"), String.class);
        constructors.put(ChecksumCalculator.calculate("vector # [ int ] = Vector int"), Integer.class);
        constructors.put(ChecksumCalculator.calculate("vector # [ long ] = Vector long"), Long.class);
        return constructors;
    }
}
