package decoder;

import api.Decoder;
import api.MessageDecoder;
import api.exceptions.DecodingException;
import decoder.primitive.IntDecoder;
import decoder.primitive.LongDecoder;

import java.io.InputStream;

/**
 * If you see it, than I've forgotten javadoc
 *
 * @author Denis Golovachev
 * @author $Author$ (current maintainer)
 * @since 1.0
 */
public class HeaderMessageDecoder implements MessageDecoder {

    private MessageDecoder nonEncryptedDecoder;

    private MessageDecoder encryptedDecoder;

    private Decoder<Long> longDecoedr = new LongDecoder();

    private Decoder<Integer> intDecoder = new IntDecoder();

    @Override
    public <T> T decode(InputStream data, Class<T> objClass) throws DecodingException {
        long authKey = longDecoedr.decode(data);
        long messageId = longDecoedr.decode(data);
        int messageLength = intDecoder.decode(data);
        if(authKey == 0) {
            return nonEncryptedDecoder.decode(data, objClass);
        }
        throw new DecodingException("encryption is not supported");
    }

    public void setNonEncryptedDecoder(MessageDecoder nonEncryptedDecoder) {
        this.nonEncryptedDecoder = nonEncryptedDecoder;
    }

    public void setEncryptedDecoder(MessageDecoder encryptedDecoder) {
        this.encryptedDecoder = encryptedDecoder;
    }
}
