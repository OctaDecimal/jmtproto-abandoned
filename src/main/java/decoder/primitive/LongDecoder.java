package decoder.primitive;

import api.Decoder;
import api.exceptions.DecodingException;

import java.io.InputStream;

/**
 * If you see it, than I've forgotten javadoc
 *
 * @author Denis Golovachev
 * @author $Author$ (current maintainer)
 * @since 1.0
 */
public class LongDecoder implements Decoder<Long> {

    private static final NumberDecoder decoder = new NumberDecoder(64);

    private static final int constructor = ChecksumCalculator.calculate("long ? = Long");

    @Override
    public Long decode(InputStream data) throws DecodingException {
        return decoder.decode(data).longValue();
    }

    @Override
    public boolean isTypeConstructor(int otherConstructor) {
        return constructor == otherConstructor;
    }
}
