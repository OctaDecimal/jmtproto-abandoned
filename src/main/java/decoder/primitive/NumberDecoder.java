package decoder.primitive;

import api.Decoder;
import api.exceptions.DecodingException;
import org.apache.commons.lang.ArrayUtils;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;

/**
 *
 */
public class NumberDecoder implements Decoder<Number> {

    private final Integer typeSize; // in bits

    public NumberDecoder(Integer size) {
        typeSize = size;
    }

    @Override
    public Number decode(InputStream stream) throws DecodingException {
        byte[] bytes = new byte[typeSize / 8]; // in bytes
        try {
            stream.read(bytes);
            ArrayUtils.reverse(bytes); // little-endian to big one
            return new BigInteger(bytes);
        } catch (IOException e) {
            throw new DecodingException("Do not like InputStream interface for ugly checked exception");
        }
    }

    @Override
    public boolean isTypeConstructor(int otherConstructor) {
        throw new UnsupportedOperationException("No type constructors for vary length fields");
    }
}
