package decoder.primitive;

import api.Decoder;
import api.exceptions.DecodingException;
import decoder.DefaultDecoderConfig;

import java.io.InputStream;
import java.util.Map;
import java.util.Vector;

/**
 * If you see it, than I've forgotten javadoc
 *
 * @author Denis Golovachev
 * @author $Author$ (current maintainer)
 * @since 1.0
 */
public class VectorDecoder implements Decoder<Vector> {

    private Map<Class, Decoder> decoderMap = DefaultDecoderConfig.decodersMap;

    private Map<Integer, Class> typeDecoders = DefaultDecoderConfig.supportedContainers();

    private Decoder<Integer> intDecoder = new IntDecoder();

    @Override
    public Vector decode(InputStream data) throws DecodingException {
        Integer constructor = intDecoder.decode(data);
        Class containerClass = typeDecoders.get(constructor);
        Decoder typeDecoder = decoderMap.get(containerClass);
        int count = intDecoder.decode(data);
        Vector output = new Vector();
        for (int i = 0; i < count; ++i) {
            Object element = typeDecoder.decode(data);
            output.add(i, element);
        }
        return output;
    }

    @Override
    public boolean isTypeConstructor(int otherConstructor) {
        return typeDecoders.keySet().contains(otherConstructor);
    }

    public void setDecoderMap(Map<Class, Decoder> decoderMap) {
        this.decoderMap = decoderMap;
    }

    public void setTypeDecoders(Map<Integer, Class> typeDecoders) {
        this.typeDecoders = typeDecoders;
    }
}
