package decoder.primitive;

import java.util.zip.CRC32;

/**
 * If you see it, than I've forgotten javadoc
 *
 * @author Denis Golovachev
 * @author $Author$ (current maintainer)
 * @since 1.0
 */
public class ChecksumCalculator {

    public static Integer calculate(String str) {
        CRC32 generator = new CRC32();
        generator.update(str.getBytes());
        return new Long(generator.getValue()).intValue();
    }
}
