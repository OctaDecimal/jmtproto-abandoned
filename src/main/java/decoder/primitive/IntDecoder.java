package decoder.primitive;

import api.Decoder;
import api.exceptions.DecodingException;

import java.io.InputStream;

/**
 * If you see it, than I've forgotten javadoc
 *
 * @author Denis Golovachev
 * @author $Author$ (current maintainer)
 * @since 1.0
 */
public class IntDecoder implements Decoder<Integer> {

    private static final NumberDecoder decoder = new NumberDecoder(32);

    private static final int constructor = ChecksumCalculator.calculate("int ? = Int");

    @Override
    public Integer decode(InputStream data) throws DecodingException {
        return decoder.decode(data).intValue();
    }

    @Override
    public boolean isTypeConstructor(int otherConstructor) {
        return constructor == otherConstructor;
    }
}
